local k = import 'ksonnet/ksonnet.beta.3/k.libsonnet';
local ingress = k.extensions.v1beta1.ingress;
local ingressTls = ingress.mixin.spec.tlsType;
local ingressRule = ingress.mixin.spec.rulesType;
local httpIngressPath = ingressRule.mixin.http.pathsType;

local kubePrometheus =
	(import 'kube-prometheus/kube-prometheus.libsonnet') + 
	(import 'lib/grafana-persistent-volume-claim.mixin.libsonnet') + 
	{
		_config+:: {
			namespace: 'monitoring',
			versions+:: {
				grafana: '5.4.3',
			},
			prometheus+:: {
				replicas: 1,
			},
			grafana+::{
				plugins:['grafana-piechart-panel','grafana-worldmap-panel'],
				replicas: 1,
			},
		},
		prometheus+:: {
			prometheus+: {
				spec+:{
					nodeSelector+: {
						'function': 'monitoring'
					},
					retention:'14d',
				storage:{
					volumeClaimTemplate:{
						"metadata": {
							"annotations": {
								"volume.beta.kubernetes.io/storage-provisioner": "kubernetes.io/aws-ebs"
							},
							"name": 'prometheus-' + $._config.prometheus.name,
							"namespace": $._config.namespace,
						},
						"spec": {
							"accessModes": [
								"ReadWriteOnce"
							],
							"resources": {
								"requests": {
									"storage": "20Gi"
								}
							},
							"storageClassName": "gp2-monitoring",
						}					
					},
				},
				}
			}
		},
		grafana+:: {
			deployment+: {
				spec+:{
					template+:{
						spec+:{
							nodeSelector+: {
								'function': 'monitoring'
							},
							securityContext:{
								runAsUser: 472,
								fsGroup: 472
							},
						},
					},
				}
			}
		},
		prometheusAdapter+:: {
			deployment+: {
				spec+:{
					replicas: 1,
					template+:{
						spec+:{
							nodeSelector+: {
								'function': 'monitoring'
							},
						},
					},
				}
			}
		},
		alertmanager+:: {
			alertmanager+: {
				spec+:{
					replicas: 1,
					template+:{
						spec+:{
							nodeSelector+: {
								'function': 'monitoring'
							},
						},
					},
				}
			}
		},
		kubeStateMetrics+:: {
			deployment+: {
				spec+:{
					replicas: 1,
					template+:{
						spec+:{
							nodeSelector+: {
								'function': 'monitoring'
							},
						},
					},
				}
			}
		},
		ingress+:: {
			'grafana':
				ingress.new() +
				ingress.mixin.metadata.withName($.grafana.deployment.metadata.name) +
				ingress.mixin.metadata.withNamespace($.grafana.deployment.metadata.namespace) +
				ingress.mixin.spec.withRules(
					ingressRule.new() +
					ingressRule.withHost('grafana.uk.birdpower.io') +
					ingressRule.mixin.http.withPaths(
					httpIngressPath.new() +
					httpIngressPath.mixin.backend.withServiceName($.grafana.service.metadata.name) +
					httpIngressPath.mixin.backend.withServicePort('http')
					),
				),
		},
	};


local kp = kubePrometheus+{
	grafana+:{
		deployment+:{
			local deployment = kubePrometheus.grafana.deployment,
			local volume = k.apps.v1beta2.deployment.mixin.spec.template.spec.volumesType,
			local container = k.apps.v1beta2.deployment.mixin.spec.template.spec.containersType,
			local containerVolumeMount = container.volumeMountsType,

			local storageVolumeName = 'grafana-' + $._config.grafana.name,
			local storageVolume = volume.fromPersistentVolumeClaim('grafana-storage',storageVolumeName),
			local storageVolumeMount = containerVolumeMount.new(storageVolumeName, '/var/lib/grafana'),

			local existingVoluems = deployment.spec.template.spec.volumes,
			spec+:{
                template+:{
                    spec+:{
                        volumes: std.filter(function(v) v.name != 'grafana-storage',deployment.spec.template.spec.volumes)+[storageVolume]
                    },
                },
            }
		},
	},
};
/* std.trace('prometheus',kp.prometheus['prometheus']) */

{ ['00namespace-' + name]: kp.kubePrometheus[name] for name in std.objectFields(kp.kubePrometheus) } +
{ ['0prometheus-operator-' + name]: kp.prometheusOperator[name] for name in std.objectFields(kp.prometheusOperator) } +
{ ['node-exporter-' + name]: kp.nodeExporter[name] for name in std.objectFields(kp.nodeExporter) } +
{ ['kube-state-metrics-' + name]: kp.kubeStateMetrics[name] for name in std.objectFields(kp.kubeStateMetrics) } +
{ ['alertmanager-' + name]: kp.alertmanager[name] for name in std.objectFields(kp.alertmanager) } +
{ ['prometheus-' + name]: kp.prometheus[name] for name in std.objectFields(kp.prometheus) } +
{ ['prometheus-adapter-' + name]: kp.prometheusAdapter[name] for name in std.objectFields(kp.prometheusAdapter) } +
{ ['grafana-' + name]: kp.grafana[name] for name in std.objectFields(kp.grafana) } + 
{ ['ingress-' + name]: kp.ingress[name] for name in std.objectFields(kp.ingress) }

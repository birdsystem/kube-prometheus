{
    _config+:: {
        namespace: 'default',
        grafana+:: {
            name: 'k8s',
            deployment:{},
        },
    },
    grafana+::{
        PersistentVolumeClaim:{
            "apiVersion": "v1",
            "kind": "PersistentVolumeClaim",
            "metadata": {
                "annotations": {
                    "volume.beta.kubernetes.io/storage-provisioner": "kubernetes.io/aws-ebs"
                },
                "name": 'grafana-' + $._config.grafana.name,
                "namespace": $._config.namespace,
            },
            "spec": {
                "accessModes": [
                    "ReadWriteOnce"
                ],
                "resources": {
                    "requests": {
                        "storage": "20Gi"
                    }
                },
                "storageClassName": "gp2-monitoring",
            }
        },
    }
}
